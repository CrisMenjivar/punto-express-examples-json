import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import ListField from "../../forms/ListField";
import TextAreaJson from "../../forms/TextAreaJson";
import useExpressForm from "../../hooks/useExpressForm";
import { responsePuntoExpressFactura } from "../../utils";

export const FormTypes = {
  SIMPLE_FORM: "SIMPLE_FORM",
  QUERY_FORM: "QUERY_FORM",
  PAYMENT_FORM: "PAYMENT_FORM",
};

function ShowFactura() {
  const {
    parseJsonPuntoExpress,
    fields,
    responseDataJson,
    typeForm,
    setFields,
    getJsonParsedRequest,
  } = useExpressForm();

  const navigate = useNavigate();

  useEffect(() => {
    parseJsonPuntoExpress(responsePuntoExpressFactura);
  }, [parseJsonPuntoExpress]);

  const handlerSubmit = (e) => {
    e.preventDefault();

    const { jsonParsedSuccess, error } = getJsonParsedRequest();

    if (error) {
      alert("Algo salio mal");
      console.log(error);
      return;
    }

    console.log("Success!!");
    console.log(jsonParsedSuccess);
  };

  return (
    <div className="App">
      {responseDataJson && (
        <>
          <div style={{ width: "50%" }}>
            <h4>Formulario: {typeForm}</h4>

            <form onSubmit={handlerSubmit}>
              {fields?.length > 0 && (
                <ListField
                  fields={fields}
                  onChangeFields={(newFields) => setFields(newFields)}
                />
              )}

              <button style={{ marginTop: 20, marginBottom: 10 }} type="submit">
                Parsear
              </button>

              <button
                style={{ marginTop: 20, marginBottom: 10 }}
                onClick={() => navigate("/comprobante")}
              >
                ver comprobante
              </button>
            </form>

            <div>
              <div>Tipos de campos:</div>
              <pr style={{ fontSize: 10 }}>
                {JSON.stringify(
                  fields.map((field) => ({
                    type: field.type,
                    typeControl: field.value.type,
                    value: field.value.val,
                  })),
                  null,
                  2
                )}
              </pr>
            </div>
          </div>

          <div style={{ width: "50%" }}>
            <TextAreaJson
              data={responseDataJson}
              applyChanges={(data) => parseJsonPuntoExpress(data)}
            />
          </div>
        </>
      )}
    </div>
  );
}

export default ShowFactura;
