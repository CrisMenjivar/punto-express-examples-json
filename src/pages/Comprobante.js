import { useNavigate } from "react-router-dom";

const Comprobante = () => {
  const navigate = useNavigate();
  return (
    <div className="App">
      <div>
        <h2>COMPROBANTE DE PAGO:</h2>

        {/* create table de factura */}

        <table>
          <thead>
            <tr>
              <th>Cantidad</th>
              <th>Descripción</th>
              <th>Precio</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Pago de colectores</td>
              <td>$100</td>
              <td>$100</td>
            </tr>
          </tbody>
        </table>

        {/* navigate to "/" */}
        <button
          style={{ marginTop: 20, marginBottom: 10 }}
          onClick={() => navigate("/")}
        >
          Hacer otra factura
        </button>
      </div>
    </div>
  );
};

export default Comprobante;
