import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import SimpleForm from "../../forms/SimpleForm";
import TextAreaJson from "../../forms/TextAreaJson";
import useExpressMultiForms from "../../hooks/useExpressMultiForms";
import { FormTypes, responsePuntoExpressMultiform } from "../../utils";

function ListForm() {
  const navigate = useNavigate();

  const {
    parseJsonPuntoExpress,
    forms,
    setForms,
    responseDataJson,
    getJsonParsedRequest,
  } = useExpressMultiForms();

  useEffect(() => {
    parseJsonPuntoExpress(responsePuntoExpressMultiform);
  }, [parseJsonPuntoExpress]);

  const handlerSubmit = (e) => {
    e.preventDefault();

    const { jsonParsedSuccess, error } = getJsonParsedRequest();

    if (error) {
      alert("Algo salio mal");
      console.log(error);
      return;
    }

    console.log("Success!!");
    console.log(jsonParsedSuccess);
  };

  const handlerChangeForm = (newForm) => {
    const newForms = forms.map((form) => {
      if (form.keyNameParam === newForm.keyNameParam) {
        return newForm;
      }
      return form;
    });
    setForms(newForms);
  };

  return (
    <div className="App">
      {responseDataJson && (
        <>
          <div style={{ width: "50%" }}>
            <h4>Formulario:</h4>

            <form onSubmit={handlerSubmit}>
              {forms?.length > 0 &&
                forms.map((form, index) => (
                  <>
                    {form?.type === FormTypes.SIMPLE_FORM && (
                      <SimpleForm
                        key={index}
                        form={form}
                        onChangeForm={(newForm) => handlerChangeForm(newForm)}
                      />
                    )}
                  </>
                ))}

              <button style={{ marginTop: 20, marginBottom: 10 }} type="submit">
                Parsear
              </button>
              <button
                style={{ marginTop: 20, marginBottom: 10 }}
                onClick={() => navigate("/show-factura-multi")}
              >
                Ir a siguiente
              </button>
            </form>

            <div>
              <div>Tipos de campos:</div>
              <pr style={{ fontSize: 10 }}>{JSON.stringify(forms, null, 2)}</pr>
            </div>
          </div>

          <div style={{ width: "50%" }}>
            <TextAreaJson
              data={responseDataJson}
              applyChanges={(data) => parseJsonPuntoExpress(data)}
            />
          </div>
        </>
      )}
    </div>
  );
}

export default ListForm;
