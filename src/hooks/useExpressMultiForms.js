import { useCallback, useState } from "react";

const useExpressMultiForms = () => {
  const [responseDataJson, setResponseDataJson] = useState(null);
  const [forms, setForms] = useState([]);

  const parseJsonPuntoExpress = useCallback((response) => {
    try {
      const { service } = response;
      // ombre del tipo de formulario: queryForm,
      const formularioKeys = Object.keys(service.dataSets.data);
      // formulario final

      // creando la lista de inputs, etc
      const forms = formularioKeys.map((key) => {
        return {
          keyNameParam: key,
          ...service.dataSets.data[key],
        };
      }); // fin map

      setForms(forms);
      setResponseDataJson(response);
    } catch (error) {
      alert("Error al parsear el formulario");
    }
  }, []);

  const getJsonParsedRequest = useCallback(() => {
    try {
      // TODO: sumar los campos de type MONEY a total_payment
      // mapped los forms
      const formsMapped = forms.map(({ keyNameParam, ...rest }) => {
        return {
          [keyNameParam]: {
            ...rest,
          },
        };
      });

      const formsObj = Object.assign({}, ...formsMapped);

      const requestDataToSend = {
        ...responseDataJson,
        service: {
          ...responseDataJson.service,
          dataSets: {
            ...responseDataJson.service.dataSets,
            status: "REQUEST",
            data: {
              ...responseDataJson.service.dataSets.data,
              ...formsObj,
            },
          },
        },
      };

      // set new fields to form
      setResponseDataJson(requestDataToSend);

      return {
        jsonParsedSuccess: requestDataToSend,
      };
    } catch (error) {
      return {
        error,
      };
    }
  }, [forms, responseDataJson]);

  return {
    responseDataJson,
    forms,
    setForms: (newForms = []) => setForms(newForms),
    setResponseDataJson,
    parseJsonPuntoExpress,
    getJsonParsedRequest,
  };
};

export default useExpressMultiForms;
