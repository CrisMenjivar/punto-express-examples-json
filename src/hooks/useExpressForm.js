import { useCallback, useState } from "react";

const useExpressForm = () => {
  const [responseDataJson, setResponseDataJson] = useState(null);
  const [fields, setFields] = useState([]);
  const [formularioTypeKey, setFormularioTypeKey] = useState("");
  const [typeForm, setTypeForm] = useState("");

  const parseJsonPuntoExpress = useCallback((response) => {
    try {
      const { service } = response;
      // ombre del tipo de formulario: queryForm,
      const formularioKey = Object.keys(service.dataSets.data)[0];
      // formulario final
      const formulario = service.dataSets.data[formularioKey];
      // campos de el formulario
      const keysFieldsForm = Object.keys(formulario.fields);

      // creando la lista de inputs, etc
      const fieldsParsed = keysFieldsForm
        .map((key) => {
          return {
            keyNameParam: key,
            ...formulario.fields[key],
          };
        }) // fin map
        .sort((a, b) => a.index - b.index); // ordenando los campos por el index

      setResponseDataJson(response); // formulario
      setFormularioTypeKey(formularioKey);
      setTypeForm(formulario.type);
      setFields(fieldsParsed); // campos del formulario
    } catch (_) {
      alert("Error al parsear el formulario");
    }
  }, []);

  const getJsonParsedRequest = useCallback(() => {
    try {
      const fieldsMapped = fields.map(({ keyNameParam, ...rest }) => {
        return {
          [keyNameParam]: {
            ...rest,
          },
        };
      });

      // convert array to object
      const fieldsObj = Object.assign({}, ...fieldsMapped);

      const requestDataToSend = {
        ...responseDataJson,
        service: {
          ...responseDataJson.service,
          dataSets: {
            ...responseDataJson.service.dataSets,
            status: "REQUEST",
            data: {
              ...responseDataJson.service.dataSets.data,
              [formularioTypeKey]: {
                ...responseDataJson.service.dataSets.data[formularioTypeKey],
                fields: fieldsObj,
              },
            },
          },
        },
      };

      // set new fields to form
      setResponseDataJson(requestDataToSend);

      return {
        jsonParsedSuccess: requestDataToSend,
      };
    } catch (error) {
      return {
        error,
      };
    }
  }, [fields, responseDataJson, formularioTypeKey]);

  return {
    parseJsonPuntoExpress,
    responseDataJson,
    fields,
    formularioTypeKey,
    typeForm,
    setFields: (newFields = []) => setFields(newFields),
    setResponseDataJson,
    getJsonParsedRequest,
  };
};

export default useExpressForm;
