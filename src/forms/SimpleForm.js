import { useEffect, useState } from "react";
import SelectBox from "./SelectBox";

// TODO: AUTOMATIC_CAPTURE este campo se hará?
const TypeInputs = {
  INPUT: "INPUT", // Campo generalmente de tipo texto, al que se le puede aplicar mascaras.
  OUTPUT: "OUTPUT", // Este campo se utiliza con la propiedad readOnly, porque solo es para proporcionar información al cliente. No se esperar que se complete el campo.
  SELECTBOX: "SELECTBOX", // Es el campo para selección generalmente tipo combo.
  HIDDEN: "HIDDEN", // Campo oculto, que no se debe presentar al cliente, pero se debe conservar en el mensaje.
  AUTOMATIC_CAPTURE: "AUTOMATIC_CAPTURE", // Campo que acepta la captura por medio de la lectora de códigos de barra. Aplica para Corresponsales Financieros
  SELECTABLE: "SELECTABLE", // Este tipo de campo debe interpretarse de la misma forma que un OUTPUT, ya que se mantiene solo como compatibilidad a versiones anteriores del API de PUNTOXPRESS.
};

const SimpleForm = ({
  form,
  onChangeForm = (newForm) => {
    console.log(newForm);
  },
}) => {
  const [fields, setFields] = useState([]);
  const [selectBoxOption, setSelectBoxOption] = useState(null);

  useEffect(() => {
    const keysFieldsForm = Object.keys(form.fields);
    // creando la lista de inputs, etc
    const fieldsParsed = keysFieldsForm
      .map((key) => {
        return {
          keyNameParam: key,
          ...form.fields[key],
        };
      }) // fin map
      .sort((a, b) => a.index - b.index); // ordenando los campos por el index

    setFields(fieldsParsed); // campos del formulario
  }, [form]);

  const onChangeSelectBox = (newFieldSelectBox, index) => {
    const newFields = fields.map((field) => {
      if (field.index === index) {
        return {
          ...field,
          value: {
            ...field.value,
            val: {
              ...newFieldSelectBox.value.val,
            },
          },
        };
      }
      return field;
    });

    const fieldsMapped = newFields.map(({ keyNameParam, ...rest }) => {
      return {
        [keyNameParam]: {
          ...rest,
        },
      };
    });

    // convert array to object
    const fieldsObj = Object.assign({}, ...fieldsMapped);

    // set new fields to form and update parent
    onChangeForm({
      ...form,
      fields: fieldsObj,
    });
  };

  const onChangeField = (text, index) => {
    const newFields = fields.map((field) => {
      if (field.index === index) {
        return {
          ...field,
          value: {
            ...field.value,
            val: text,
          },
        };
      }
      return field;
    });

    const fieldsMapped = newFields.map(({ keyNameParam, ...rest }) => {
      return {
        [keyNameParam]: {
          ...rest,
        },
      };
    });

    // convert array to object
    const fieldsObj = Object.assign({}, ...fieldsMapped);

    // set new fields to form and update parent
    onChangeForm({
      ...form,
      fields: fieldsObj,
    });
  };

  const isReference = ({ keyNameParam }) => keyNameParam === "referencia";

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        gap: "10px",
      }}
    >
      {fields?.length > 0 &&
        fields.map((field, index) => (
          <div key={index}>
            {!isReference(field) && field?.type === TypeInputs.INPUT && (
              <>
                {field?.label && <div>{field.label}</div>}
                <input
                  // placeholder={field.tooltip}
                  required={field.required}
                  name={`${field?.name}`} // use index to get the name
                  title={field?.tooltip || ""}
                  type={field.value.type}
                  value={field?.value.val}
                  disabled={!field?.value?.editable}
                  onChange={({ target }) =>
                    onChangeField(target.value, field.index)
                  }
                />
              </>
            )}

            {!isReference(field) && field?.type === TypeInputs.OUTPUT && (
              <>
                {field?.label && <div>{field.label}</div>}
                <input
                  // placeholder={field.tooltip}
                  required={field.required}
                  title={field?.tooltip || ""}
                  type={"TEXT"}
                  value={field?.value.val}
                  disabled
                />
              </>
            )}

            {/* SELECTABLE es igual que el OUTPUT */}
            {!isReference(field) && field?.type === TypeInputs.SELECTABLE && (
              <>
                {field?.label && <div>{field.label}</div>}
                <input
                  // placeholder={field.tooltip}
                  required={field.required}
                  title={field?.tooltip || ""}
                  type={"TEXT"}
                  value={field?.value.val}
                  disabled
                />
              </>
            )}

            {/* SELECT BOX */}
            {field?.type === TypeInputs.SELECTBOX && (
              <SelectBox
                field={field}
                onSelectOption={setSelectBoxOption}
                onChangeField={(field) => onChangeSelectBox(field, field.index)}
              />
            )}

            {/* REFERENCIA */}
            {isReference(field) && (
              <>
                <div>{selectBoxOption?.label || "Referencia"}</div>
                <input
                  // placeholder={field.tooltip}
                  required={field.required}
                  name={`${field?.name}`} // use index to get the name
                  title={field?.tooltip || ""}
                  type={field.value.type}
                  value={field?.value.val}
                  disabled={!field?.value?.editable}
                  onChange={({ target }) =>
                    onChangeField(target.value, field.index)
                  }
                />
              </>
            )}
          </div>
        ))}
    </div>
  );
};

export default SimpleForm;
