import React, { useEffect, useState } from "react";

const TextAreaJson = ({ data = {}, applyChanges }) => {
  const [text, setText] = useState("");

  useEffect(() => {
    setText(JSON.stringify(data, null, 2));
  }, [data]);

  const handleChange = ({ value }) => {
    setText(value);
  };

  return (
    <>
      <div>JSON a enviar:</div>

      {/* text area */}
      <textarea
        style={{
          width: "100%",
          height: "70vh",
          backgroundColor: "transparent",
          fontSize: 9,
          color: "#00C897",
        }}
        value={text}
        onChange={({ target }) => handleChange(target)}
      />

      <button onClick={() => applyChanges(JSON.parse(text))}>
        Aplicar cambios
      </button>
    </>
  );
};

export default TextAreaJson;
