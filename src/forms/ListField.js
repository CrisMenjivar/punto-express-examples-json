const TypeInputs = {
  INPUT: "INPUT",
  OUTPUT: "OUTPUT",
};

const ListField = ({ fields = [], onChangeFields = () => {} }) => {
  const onChangeField = (text, index) => {
    const newFields = fields.map((field) => {
      if (field.index === index) {
        return {
          ...field,
          value: {
            ...field.value,
            val: text,
          },
        };
      }
      return field;
    });

    onChangeFields(newFields);
  };

  return (
    <>
      {fields?.length > 0 &&
        fields.map((field, index) => (
          <div key={index}>
            {field?.type === TypeInputs.INPUT && (
              <>
                {field?.label && <div>{field.label}</div>}
                <input
                  placeholder={field.tooltip}
                  required={field.required}
                  name={`${field?.name}`} // use index to get the name
                  title={field?.tooltip || ""}
                  type={field.value.type}
                  value={field?.value.val}
                  disabled={!field?.value?.editable}
                  onChange={({ target }) =>
                    onChangeField(target.value, field.index)
                  }
                />
              </>
            )}

            {field?.type === TypeInputs.OUTPUT && (
              <>
                {field?.label && <div>{field.label}</div>}
                <input
                  placeholder={field.tooltip}
                  required={field.required}
                  title={field?.tooltip || ""}
                  type={"TEXT"}
                  value={field?.value.val}
                  disabled
                />
              </>
            )}
          </div>
        ))}
    </>
  );
};

export default ListField;
