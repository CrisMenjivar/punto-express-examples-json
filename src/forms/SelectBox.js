import React, { useEffect, useState } from "react";

// obtener la fecha mas proxima de un listado []

const SelectBox = ({ field, onChangeField, onSelectOption }) => {
  const [options, setOptions] = useState([]);
  const [indexSelected, setIndexSelected] = useState(null);

  useEffect(() => {
    const optionSelect = options.find(
      (option) => String(option["index"]) === String(indexSelected)
    );

    if (optionSelect) {
      onSelectOption(optionSelect);
      const { keyParamName, ...rest } = optionSelect;
      onChangeField({
        ...field,
        value: {
          ...field.value,
          val: {
            [keyParamName]: {
              ...rest,
            },
          },
        },
      });
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [indexSelected, onSelectOption]);

  useEffect(() => {
    try {
      const { val: optionsValue } = field.value;
      const keys = Object.keys(optionsValue);

      const options = keys.map((key) => {
        return {
          keyParamName: key,
          ...optionsValue[key],
        };
      });

      setOptions(options);
      setIndexSelected(options[0].index);
    } catch (error) {
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <div>Selecciona una opción</div>
      <select
        name={`${field?.name}`} // use index to get the name
        title={field?.tooltip || ""}
        // value={indexSelected}
        required={field.required}
        onChange={({ target }) => setIndexSelected(target.value)}
      >
        {options?.length > 0 &&
          options?.map((option) => (
            <>
              <option
                selected={indexSelected === option.index}
                key={option.index}
                value={option.index}
              >
                {option.label}
              </option>
            </>
          ))}
      </select>
    </div>
  );
};

export default SelectBox;
