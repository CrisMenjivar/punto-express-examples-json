import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Comprobante from "./pages/Comprobante";
import SearchForm from "./pages/simple-forms/SearchForm";
import ShowFactura from "./pages/simple-forms/ShowFactura";
import ShowFacturaMulti from "./pages/multi-form/ShowFacturaMulti";
import SearchMultiForm from "./pages/multi-form/ListForm";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          {/* respuesta con 1 formulario */}
          <Route path="/" element={<SearchMultiForm />}></Route>
          <Route path="/search-form" element={<SearchForm />}></Route>
          <Route path="/show-factura" element={<ShowFactura />}></Route>
          <Route path="/comprobante" element={<Comprobante />}></Route>

          {/* respuesta con mas de 1 formulario */}
          <Route
            path="/show-factura-multi"
            element={<ShowFacturaMulti />}
          ></Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
